# Pebble

Pebble is a general-purpose Discord bot.

## License

Copyright 2022 Sam (starshines)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

### Acknowledgments

- `pebble/core/paginator.py` is mostly based on [R. Danny's paginator][rdanny_paginator] and is licensed under the Mozilla Public License, 2.0

[rdanny_paginator]: https://github.com/Rapptz/RoboDanny/blob/cd23371cee697d861182a037f90896d9d4a082dd/cogs/utils/paginator.py
