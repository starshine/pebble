# Copyright 2022 Sam (starshines)
# SPDX-License-Identifier: Apache-2.0

import asyncpg
import discord
from discord.ext import commands

from core import Pebble, require_permission, PermissionLevel


class Meta(commands.Cog):
    """I'm so meta, even this acronym..."""

    _bot: Pebble
    _db: asyncpg.Pool

    def __init__(self, bot: Pebble):
        self._bot = bot
        self._db = bot.db

    @commands.hybrid_command()
    @require_permission(PermissionLevel.USER)
    async def ping(self, ctx: commands.Context):
        """A nice game of tennis. Ping pong!"""

        return await ctx.send("Pong!")


async def setup(bot: Pebble):
    await bot.add_cog(Meta(bot))
