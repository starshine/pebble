# Copyright 2022 Sam (starshines)
# SPDX-License-Identifier: Apache-2.0

import asyncpg
import discord
from core import (
    Context,
    Pebble,
    PermissionLevel,
    require_permission,
)
from discord.ext import commands


class Config(commands.Cog):
    """Configuration commands"""

    _bot: Pebble
    _db: asyncpg.Pool

    def __init__(self, bot: Pebble):
        self._bot = bot
        self._db = bot.db

    @commands.hybrid_group(fallback="show")
    @require_permission(PermissionLevel.MANAGER)
    async def config(self, ctx: Context):
        """Show this server's current configuration."""

        row = await ctx.pool.fetchrow(
            "select manager_roles, moderator_roles, mod_log from guilds where id = $1",
            ctx.guild.id,
        )

        embed = discord.Embed(
            title=f"Configuration for {ctx.guild.name}",
            colour=discord.Colour.blurple(),
        )

        manager_roles = [ctx.guild.get_role(id) for id in row["manager_roles"]]
        manager_roles = [role for role in manager_roles if role is not None]

        # add any roles that:
        # - have manage guild or administrator
        # - are not managed by a bot
        manager_roles.extend(
            [
                role
                for role in ctx.guild.roles
                if (
                    (role.permissions.administrator or role.permissions.manage_guild)
                    and role not in manager_roles
                    and not role.is_bot_managed()
                )
            ]
        )

        embed.add_field(
            name="Manager roles",
            value=", ".join([r.mention for r in manager_roles])
            if len(manager_roles) > 0
            else "None",
            inline=False,
        )

        moderator_roles = [ctx.guild.get_role(id) for id in row["moderator_roles"]]
        moderator_roles = [role for role in moderator_roles if role is not None]

        # add any roles that:
        # - have manage messages
        # - are not manager roles (have admin, have manage guild, or are explicitly set as manager role)
        # - are not managed by a bot
        moderator_roles.extend(
            [
                role
                for role in ctx.guild.roles
                if (
                    role.permissions.manage_messages
                    and role not in manager_roles
                    and role not in moderator_roles
                    and not (
                        role.permissions.administrator
                        or role.permissions.manage_guild
                        or role.is_bot_managed()
                    )
                )
            ]
        )

        embed.add_field(
            name="Moderator roles",
            value=", ".join([r.mention for r in moderator_roles])
            if len(moderator_roles) > 0
            else "None",
            inline=False,
        )

        embed.add_field(
            name="Mod log channel",
            value=f"<#{row['mod_log']}>" if row["mod_log"] else "None",
            inline=False,
        )

        return await ctx.send(embeds=[embed], ephemeral=True)

    @config.group()
    async def managers(self, ctx: Context):
        pass

    @managers.command(name="add")
    @require_permission(PermissionLevel.ADMIN)
    async def add_manager(self, ctx: Context, role: discord.Role):
        """Add a manager role."""

        row = await ctx.pool.fetchrow(
            "select manager_roles, moderator_roles from guilds where id = $1",
            ctx.guild.id,
        )

        if role.permissions.administrator:
            return await ctx.send(
                f"{role.mention} is already an admin role. "
                + "Admins can execute any command.",
                ephemeral=True,
            )

        if role.id in row["manager_roles"] or role.permissions.manage_guild:
            return await ctx.send(
                f"{role.mention} is already a manager role. "
                + 'Any role with the "Manage Server" permissions is automatically treated as a manager role.',
                ephemeral=True,
            )

        if role.id in row["moderator_roles"]:
            resp = await ctx.confirm(
                content=f"{role.mention} is already a moderator role.\n"
                + "Do you want to make it a manager role instead?",
                ephemeral=False,
            )
            if not resp:
                return

        await ctx.pool.execute(
            """update guilds
            set manager_roles = array_append(manager_roles, $1),
            moderator_roles = array_remove(moderator_roles, $1)
            where id = $2""",
            role.id,
            ctx.guild.id,
        )

        await ctx.send(f"Added {role.mention} as a manager role.")

    @managers.command(name="remove")
    @require_permission(PermissionLevel.ADMIN)
    async def remove_manager(self, ctx: Context, role: discord.Role):
        """Remove a manager role."""

        row = await ctx.pool.fetchrow(
            "select manager_roles, manager_roles from guilds where id = $1",
            ctx.guild.id,
        )

        if role.id not in row["manager_roles"]:
            return await ctx.send(
                f"{role.mention} is not a manager role.\n"
                + 'Note that any role with the "Manage Server" or "Administrator" permission is automatically treated as a manager role, '
                + "and cannot be removed.",
                ephemeral=True,
            )

        await ctx.pool.execute(
            "update guilds set manager_roles = array_remove(manager_roles, $1) where id = $2",
            role.id,
            ctx.guild.id,
        )

        return await ctx.send(f"Removed {role.mention} as a manager role.")

    @config.group()
    async def moderators(self, ctx: Context):
        pass

    @moderators.command(name="add")
    @require_permission(PermissionLevel.ADMIN)
    async def add_moderator(self, ctx: Context, role: discord.Role):
        """Add a moderator role."""

        row = await ctx.pool.fetchrow(
            "select manager_roles, moderator_roles from guilds where id = $1",
            ctx.guild.id,
        )

        if role.permissions.administrator:
            return await ctx.send(
                f"{role.mention} is already an admin role. "
                + "Admins can execute any command.",
                ephemeral=True,
            )

        if role.id in row["manager_roles"] or role.permissions.manage_guild:
            return await ctx.send(
                f"{role.mention} is already a manager role. "
                + "Managers can execute any command that requires a moderator role.",
                ephemeral=True,
            )

        if role.id in row["moderator_roles"] or role.permissions.manage_messages:
            return await ctx.send(
                f"{role.mention} is already a moderator role. "
                + 'Note that any role with the "Manage Messages" permission is automatically treated as a moderator role.',
                ephemeral=True,
            )

        await ctx.pool.execute(
            "update guilds set moderator_roles = array_append(moderator_roles, $1) where id = $2",
            role.id,
            ctx.guild.id,
        )

        return await ctx.send(f"Added {role.mention} as a moderator role.")

    @moderators.command(name="remove")
    @require_permission(PermissionLevel.ADMIN)
    async def remove_moderator(self, ctx: Context, role: discord.Role):
        """Remove a moderator role."""

        row = await ctx.pool.fetchrow(
            "select manager_roles, moderator_roles from guilds where id = $1",
            ctx.guild.id,
        )

        if role.id not in row["moderator_roles"]:
            return await ctx.send(
                f"{role.mention} is not a moderator role.\n"
                + 'Note that any role with the "Manage Messages" permission is automatically treated as a moderator role, '
                + "and cannot be removed.",
                ephemeral=True,
            )

        await ctx.pool.execute(
            "update guilds set moderator_roles = array_remove(moderator_roles, $1) where id = $2",
            role.id,
            ctx.guild.id,
        )

        return await ctx.send(f"Removed {role.mention} as a moderator role.")

    @config.command()
    @require_permission(PermissionLevel.MANAGER)
    async def modlog(self, ctx: Context, channel: discord.TextChannel):
        """Set the moderation log channel."""

        if channel.guild.id != ctx.guild.id:
            return await ctx.send(
                f'\N{CROSS MARK} Channel "{channel.mention}" not found.',
                ephemeral=True,
            )

        user_perms = channel.permissions_for(ctx.author)
        if not (
            user_perms.view_channel
            and user_perms.send_messages
            and user_perms.embed_links
        ):
            return await ctx.send(
                "You do not have permission to send embeds in that channel.",
                ephemeral=True,
            )

        me_perms = channel.permissions_for(ctx.guild.me)
        if not (
            me_perms.view_channel and me_perms.send_messages and me_perms.embed_links
        ):
            return await ctx.send(
                "I do not have permission to send embeds in that channel.",
                ephemeral=True,
            )

        await ctx.pool.execute(
            "update guilds set mod_log = $1 where id = $2", channel.id, ctx.guild.id
        )

        return await ctx.send(
            f"Success! Moderation logs will now be sent in {channel.mention}."
        )


async def setup(bot: Pebble):
    await bot.add_cog(Config(bot))
