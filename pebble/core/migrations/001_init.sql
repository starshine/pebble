create table guilds (
    id  bigint  primary key,

    manager_roles   bigint[] not null default array[]::bigint[],
    moderator_roles bigint[] not null default array[]::bigint[],

    mod_log bigint
);

create table mod_log (
    id          serial  primary key,
    guild_id    bigint  not null,

    user_id     bigint  not null,
    mod_id      bigint  not null,

    action_type text    not null,
    reason      text    not null,

    time        timestamp with time zone   not null    default (now() at time zone 'utc'),

    -- for later editing of reasons
    channel_id  bigint,
    message_id  bigint
);

create table info (
    id int primary key not null default 1, -- enforced only equal to 1
    schema_version int,
    constraint singleton check (id = 1) -- enforce singleton table/row
);

insert into info (schema_version) values (1);
