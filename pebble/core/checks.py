# Copyright 2022 Sam (starshines)
# SPDX-License-Identifier: Apache-2.0

from discord.ext import commands

from .bot import Pebble
from .enums import PermissionLevel


def require_permission(require: PermissionLevel):
    async def predicate(ctx: commands.Context[Pebble]) -> bool:
        if not ctx.guild:
            raise commands.CheckFailure("This command cannot be used in DMs.")

        try:
            level = await _permission_for(ctx)
        except Exception as e:
            raise commands.CommandInvokeError(e)

        if level < require:
            raise commands.CheckFailure(
                f"This command requires `{require}` permissions."
            )

        return True

    return commands.check(predicate)


async def _permission_for(ctx: commands.Context[Pebble]) -> PermissionLevel:
    if not ctx.guild:
        return PermissionLevel.USER

    if ctx.author.guild_permissions.administrator:
        return PermissionLevel.ADMIN
    elif ctx.author.guild_permissions.manage_guild:
        return PermissionLevel.MANAGER
    elif ctx.author.guild_permissions.manage_messages:
        return PermissionLevel.MODERATOR

    row = await ctx.bot.db.fetchrow(
        "select manager_roles, moderator_roles from guilds where id = $1", ctx.guild.id
    )
    if not row:
        return PermissionLevel.USER

    for role in ctx.author.roles:
        if role.id in row["manager_roles"]:
            return PermissionLevel.MANAGER

    for role in ctx.author.roles:
        if role.id in row["moderator_roles"]:
            return PermissionLevel.MODERATOR

    return PermissionLevel.USER
